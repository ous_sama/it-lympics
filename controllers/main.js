var app = angular.module('myApp', []);

app.controller('myCtrl', function ($scope, $http, $sce) {
    $http.get("./assets/kunst.json")
        .then(function (response) {
            $scope.response = response.data;
        });
});